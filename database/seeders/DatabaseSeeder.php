<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;
use App\Models\Spells;
use App\Models\DNDClass;
use App\Models\DNDClassLEVEL;
use App\Models\SpellSchool;
use App\Models\Feature;

///Monolith data scraper to seed the database.
class DatabaseSeeder extends Seeder
{

    private const APIroute = "https://www.dnd5eapi.co";

    public function getData() {
        $headers = [
        'Content-Type' => 'application/json'
        ];
        $body = '{"query":"query {spells(limit:10000) {index,name,desc,range,higher_level,components,material,area_of_effect {size,type},ritual,duration,concentration,casting_time,level,attack_type,damage {damage_type {index,name}},school {index,name,desc},classes {index}},classes {index,name, hit_die, class_levels {index, level, ability_score_bonuses,prof_bonus,features {index}}}, features(limit:1000) {index,name, level,desc}}","variables":{}}';
        $res = HTTP::withHeaders($headers)->withBody($body,'application/json')->post('https://www.dnd5eapi.co/graphql');
        return $res->json()['data'];
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $data = $this->getData();

        //create features :
        foreach ($data['features'] as $key => $feature_data) {
            echo("> create featire '" . $feature_data['index'] . "'\n");

            $feature = new Feature();
            $feature->fill([
                'index' => $feature_data['index'],
                'name' => $feature_data['name'],
                'level' => $feature_data['level'],
                'desc' => implode("\n",$feature_data['desc'])
            ]);
            $feature->save();
        }

        foreach ($data['classes'] as $key => $class_data) {
            echo("> create class '" . $class_data['index'] . "'\n");
            $class = new DNDClass();

            //make levels :
            foreach($class_data['class_levels'] as $key => $class_level_data) {
                $class_level = new DNDClassLevel();
                $class_level->fill([
                    'index' => $class_level_data['index'],
                    'level' => $class_level_data['level'],
                    'ability_score_bonuses' => $class_level_data['ability_score_bonuses'],
                    'prof_bonus' => $class_level_data['prof_bonus'],
                ]);
                $class_level->save();
                $class_level->features()->sync(
                    collect($class_level_data['features'])->pluck('index')->toArray()
                );
            }

            $class->fill([
                'index' => $class_data['index'],
                'name' => $class_data['name'],
                'hit_die' => $class_data['hit_die'],
            ]);
            $class->save();
            $class->levels()->sync(collect($class_data['class_levels'])->pluck('index')->toArray());
        }

        foreach ($data['spells'] as $key => $spell_data) {
            echo("create spell '" . $spell_data['index'] . "'\n");

            //get or create school :
            $school = SpellSchool::find($spell_data['school']['index']);
            if($school) {
                //found
            } else {
                echo("> create school '" . $spell_data['school']['index'] . "'\n");
                $school = new SpellSchool();
                $school->fill($spell_data['school']);
                $school->save();
            }


            $spell = new Spells();

            $spell->fill([
                'index' => $spell_data['index'],
                'name' => $spell_data['name'],
                'desc' => implode("\n",$spell_data['desc']),
                'range' => $spell_data['range'],
                'higher_level' => is_array($spell_data['higher_level']) ? implode($spell_data['higher_level']) : $spell_data['higher_level'],
                'components' => implode(',',$spell_data['components']),
                'material' => $spell_data['material'],
                'ritual' => $spell_data['ritual'],
                'duration' => $spell_data['duration'],
                'concentration' => $spell_data['concentration'],
                'casting_time'  => $spell_data['casting_time'],
                'level'  => $spell_data['level'],
                'attack_type' => $spell_data['attack_type'],
                'area_of_effect_type' => $spell_data['area_of_effect'] ? $spell_data['area_of_effect']['type'] : null,
                'area_of_effect_size' => $spell_data['area_of_effect'] ? $spell_data['area_of_effect']['size'] : null,
                'damage' => !is_null($spell_data['damage']) && !is_null($spell_data['damage']['damage_type']) ? $spell_data['damage']['damage_type']['index'] : 'none',
                'school' => $spell_data['school']['index']
            ]);
            $spell->save();

            
            //link classes:
            foreach ($spell_data['classes'] as $key => $class_link) {
                $spell->classes()->syncWithoutDetaching([$class_link['index']]);
            }
        }

        echo("ALL DONE\n");
    }
}
