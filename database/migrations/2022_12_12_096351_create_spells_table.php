<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spells', function (Blueprint $table) {
            $table->string('index')->primary();
            $table->string('name');
            $table->text('desc');
            $table->string('range');
            $table->text('higher_level')->nullable();
            $table->string('components');
            $table->text('material')->nullable();
            $table->tinyInteger('ritual');
            $table->string('duration');
            $table->tinyInteger('concentration');
            $table->string('casting_time');
            $table->integer('level');
            $table->string('attack_type')->nullable();
            $table->string('area_of_effect_type')->nullable();
            $table->integer('area_of_effect_size')->nullable();
            $table->string('damage');
            $table->string('school'); 
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spells');
    }
};
