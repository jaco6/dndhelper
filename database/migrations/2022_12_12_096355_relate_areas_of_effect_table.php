<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('areas_of_effect_spells', function (Blueprint $table) {
            $table->foreign('spell_index')->references('index')->on('spells');
            $table->foreign('area_of_effect_type')->references('type')->on('areas_of_effect');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
