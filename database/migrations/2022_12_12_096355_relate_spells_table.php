<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spells', function (Blueprint $table) {
            $table->foreign('school')->references('index')->on('spell_schools');
        }); 

        Schema::table('classes_spells', function (Blueprint $table) {
            $table->foreign('spell_index')->references('index')->on('spells');
            $table->foreign('class_index')->references('index')->on('classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('classes_spells', function (Blueprint $table) {
            $table->dropForeign('classes_spells_spell_index_foreign');  
        });
    }
};
