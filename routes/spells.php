<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SpellsController;
/*
|--------------------------------------------------------------------------
| Spells Routes
|--------------------------------------------------------------------------
*/

Route::controller(SpellsController::class)->group(function ($router) {
    Route::get('/', [
        'uses' => 'index',
        'as' => 'spells.index'
    ]);
    Route::get('data', [
        'uses' => 'indexData',
        'as' => 'spells.index.data'
    ]);
    Route::get('/{spellIndex}', [
        'uses' => 'show',
        'as' => 'spells.show'
    ]);
});

