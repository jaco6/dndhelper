<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->name('dashboard');



Route::group(['prefix' => 'spells', 'middleware' => ['web']], function ($router) {
    require 'spells.php';
});

Route::group(['prefix' => 'schools', 'middleware' => ['web']], function ($router) {
    require 'schools.php';
});

Route::group(['prefix' => 'classes', 'middleware' => ['web']], function ($router) {
    require 'classes.php';
});