<?php

use App\Http\Controllers\DNDClassesController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Schools Routes
|--------------------------------------------------------------------------
*/

Route::controller(DNDClassesController::class)->group(function ($router) {
    Route::get('/', [
        'uses' => 'index',
        'as' => 'dndclasses.index'
    ]);
    Route::get('data', [
        'uses' => 'indexData',
        'as' => 'dndclasses.index.data'
    ]);
    Route::get('/{dndclassIndex}', [
        'uses' => 'show',
        'as' => 'dndclasses.show'
    ]);
});

