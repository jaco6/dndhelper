<?php

use App\Http\Controllers\SchoolsController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Schools Routes
|--------------------------------------------------------------------------
*/

Route::controller(SchoolsController::class)->group(function ($router) {
    Route::get('/', [
        'uses' => 'index',
        'as' => 'schools.index'
    ]);
    Route::get('data', [
        'uses' => 'indexData',
        'as' => 'schools.index.data'
    ]);
    Route::get('/{schoolIndex}', [
        'uses' => 'show',
        'as' => 'schools.show'
    ]);
});

