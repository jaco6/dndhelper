@extends('layouts.app')

@section('title', 'Viewing Spell \'' . $spell->name . '\'')
@section('heading', 'View Spell \'' . $spell->name . '\'')
@section('subheading', 'View Spell \'' . $spell->name . '\'')

@section('content')
<div class="container-fluid mt-2 pt-5 booktemplate">
    <div class="container-fluid">
        <div class="row pb-3">
            <div class="col">
                <h1 class="pt-3 spell-title">{{$spell->name}}</h1>
            </div>
        </div>

        <div class="gold"></div>

        <div>
            <div class="row">
                <div class="col">
                    <span class="spell-field">Casting Time:</span> {{$spell->casting_time}}
                </div>
            </div>
        </div>

        <div>
            <div class="row">
                <div class="col">
                    <span class="spell-field">Range:</span> {{$spell->range}}
                </div>
            </div>
        </div>

        <div>
            <div class="row">
                <div class="col">
                    <span class="spell-field">Higher Level:</span> {{$spell->higher_level}}
                </div>
            </div>
        </div>


        <div>
            <div class="row">
                <div class="col">
                    <span class="spell-field">Components:</span> {{str_replace(',',"\t",$spell->components)}}
                </div>
            </div>
        </div>

        @if ($spell->material)
            <div>
                <div class="row">
                    <div class="col">
                        <span class="spell-field">Material:</span> {{$spell->material}}
                    </div>
                </div>
            </div>
        @endif
        
        @if ($spell->aoeType)
            <div>
                <div class="row">
                    <div class="col">
                        <span class="spell-field">Area Of Effect:</span> {{$spell->aoeType}}  {{$spell->aoeSize}}
                    </div>
                </div>
            </div>
        @endif

        @if ($spell->ritual)
            <div>
                <div class="row">
                    <div class="col">
                        <span class="spell-field">Ritual</span>
                    </div>
                </div>
            </div>
        @endif

        <div>
            <div class="row">
                <div class="col">
                    <span class="spell-field">Duration:</span> {{$spell->duration}}
                </div>
            </div>
        </div>

        @if ($spell->concentration)
            <div>
                <div class="row">
                    <div class="col">
                        <span class="spell-field">Concentration</span>
                    </div>
                </div>
            </div>
        @endif

        <div>
            <div class="row">
                <div class="col">
                    <span class="spell-field">Casting Time:</span> {{$spell->casting_time}}
                </div>
            </div>
        </div>

        <div>
            <div class="row">
                <div class="col">
                    <span class="spell-field">Level:</span> {{$spell->level}}
                </div>
            </div>
        </div>

        @if ($spell->attack_type)
            <div>
                <div class="row">
                    <div class="col">
                        <span class="spell-field">Attack Type:</span> {{$spell->attack_type}}
                    </div>
                </div>
            </div>
        @endif

        <div>
            <div class="row">
                <div class="col">
                    <span class="spell-field">Damage Type:</span> {{$spell->damage}}
                </div>
            </div>
        </div>


        <div>
            <div class="row">
                <div class="col">
                    <span class="spell-field">School:</span> <a href="/schools/{{$spell->school}}">{{$spell->school}}</a>
                </div>
            </div>
        </div>

        <div>
            <div class="row">
                <div class="col">
                    <span class="spell-field">Classes:
                </div>
                @foreach ($spell->classes as $class)
                    <div class="col">
                        </span><a href="/classes/{{$class->index}}">{{$class->name}}</a></span>
                    </div>
                @endforeach
            </div>
            
        </div>

        <div>
            <div class="row pt-3">
                <div class="col">
                    <span class="spell-field">Description:
                        <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#desc-panel" aria-expanded="false" aria-controls="#desc-panel">
                            Show/Hide
                        </button>
                    </span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="collapse" id="desc-panel">
                    <div class="card card-body booktemplate">
                        @foreach (explode("\n",$spell->desc) as $line)
                            <p>{{$line}}</p>
                        @endforeach
                    </div>
                  </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <a class="btn btn-primary" type="button" href="{{ url()->previous() }}">Back</a>
            </div>
        </div>
       
    </div>
</div>


@endsection