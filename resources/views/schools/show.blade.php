@extends('layouts.app')

@section('title', 'Viewing School \'' . $school->name . '\'')
@section('heading', 'View School \'' . $school->name . '\'')
@section('subheading', 'View School \'' . $school->name . '\'')

@section('content')
<div class="container-fluid mt-2 pt-5 booktemplate">
    <div class="container-fluid">
        <div class="row pb-3">
            <div class="col">
                <h1 class="pt-3 school-title">{{$school->name}}</h1>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card card-body booktemplate">
                    @foreach (explode("\n",$school->desc) as $line)
                        <p>{{$line}}</p>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <a class="btn btn-primary" type="button" href="{{ url()->previous() }}">Back</a>
            </div>
        </div>
       
    </div>
</div>
@endsection