@extends('layouts.app')

@section('title', 'Viewing Class \'' . $class->name . '\'')
@section('heading', 'View Class \'' . $class->name . '\'')
@section('subheading', 'View Class \'' . $class->name . '\'')

@section('content')
<div class="container-fluid mt-2 pt-5 booktemplate">
    <div class="container-fluid">
        <div class="row pb-3">
            <div class="col">
                <h1 class="pt-3 class-title">{{$class->name}}</h1>
            </div>
        </div>

        <div>
            <div class="row">
                <div class="col">
                    <span class="class-field">Hit Die:</span> 1d{{$class->hit_die}} per level
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <a class="btn btn-primary" type="button" href="{{ url()->previous() }}">Back</a>
            </div>
        </div>
       
    </div>
</div>
@endsection