@extends('layouts.app')


@section('title', 'All Classes')

@section('content')
<div class="container-fluid mt-2 pt-5 booktemplate">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <table class="table" id="table">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">name</th>
                            <th class="text-center">actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
    </div>
</div>

<script type="text/javascript">
    var table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('dndclasses.index.data') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'actions', name: 'actions', orderable: false, searchable: false},
        ],
    });
</script>
@endsection