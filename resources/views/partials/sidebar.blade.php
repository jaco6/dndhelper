<!DOCTYPE html>
<nav class="navbar navbar-dark bg-dark fixed-top">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#sidebar-max" aria-controls="offcanvasNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">
            @yield("title")
        </a>

        <div class="offcanvas-body">
        <div class="offcanvas text-bg-dark offcanvas-start" tabindex="-1" id="sidebar-max" aria-labelledby="offcanvasLabel">
            <div class="offcanvas-header">
                <h5 class="offcanvas-title" id="sidebarMaxLabel">DND Helper</h5>
                <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="/classes">Classes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/spells">Spells</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/schools">Schools</a>
                    </li>
            </div>
        </div>
    </div>
</nav>
