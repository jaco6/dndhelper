<?php

namespace App\Http\Controllers;

use App\Models\Spells;

use Yajra\DataTables\DataTables;

class SpellsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('spells.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexData()
    {
        $spells = spells::all();
        return DataTables::of($spells)
            ->addIndexColumn()
            ->addColumn('actions', function ($spell) {
                return '&nbsp;&nbsp;<a class="btn btn-success btn-xs" href="/spells/' . $spell->index . '"><i class="fa fa-file-o fa-fw"></i> View</a>';
            })
            ->escapeColumns()
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $spell
     * @return \Illuminate\Http\Response
     */
    public function show(string $spell)
    {
        $spell = spells::where('index',$spell)->first();
        return view('spells.show')->withSpell($spell);
    }
}
