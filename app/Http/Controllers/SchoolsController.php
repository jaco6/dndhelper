<?php

namespace App\Http\Controllers;

use App\Models\SpellSchool;

use Yajra\DataTables\DataTables;

class SchoolsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('schools.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexData()
    {
        $schools = SpellSchool::all();
        return DataTables::of($schools)
            ->addIndexColumn()
            ->addColumn('actions', function ($spell) {
                return '&nbsp;&nbsp;<a class="btn btn-success btn-xs" href="/schools/' . $spell->index . '"><i class="fa fa-file-o fa-fw"></i> View</a>';
            })
            ->escapeColumns()
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $school
     * @return \Illuminate\Http\Response
     */
    public function show(string $school_index)
    {
        $school = SpellSchool::where('index',$school_index)->first();
        return view('schools.show')->withSchool($school);
    }

}
