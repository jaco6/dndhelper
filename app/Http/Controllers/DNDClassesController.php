<?php

namespace App\Http\Controllers;

use App\Models\DNDClass;

use Yajra\DataTables\DataTables;

class DNDClassesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dndclasses.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexData()
    {
        $dndclasses = DNDClass::all();
        return DataTables::of($dndclasses)
            ->addIndexColumn()
            ->addColumn('actions', function ($spell) {
                return '&nbsp;&nbsp;<a class="btn btn-success btn-xs" href="/classes/' . $spell->index . '"><i class="fa fa-file-o fa-fw"></i> View</a>';
            })
            ->escapeColumns()
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $class
     * @return \Illuminate\Http\Response
     */
    public function show(string $class_index)
    {
        $class = DNDClass::where('index',$class_index)->first();
        return view('dndclasses.show')->withClass($class);
    }

}
