<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

use function PHPSTORM_META\map;

class DNDClassLevel extends Model
{
    use HasFactory;

    protected $table = "class_levels";

    protected $primaryKey = 'index';
    public $incrementing = false;

    protected $fillable = [
        'index',
        'level',
        'ability_score_bonuses',
        'prof_bonus'
    ];

    /**
     * Get all of the features for the class level
     */
    public function features()
    {
        return $this->belongsToMany(Feature::class,'class_levels_features','class_level_index','feature_index');
    }
}
