<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spells extends Model
{
    use HasFactory;

    protected $primaryKey = 'index';
    public $incrementing = false;

    protected $fillable = [
        'index',
        'name',
        'desc',
        'range',
        'higher_level',
        'components',
        'material',
        'area_of_effect',
        'ritual',
        'duration',
        'concentration',
        'casting_time',
        'level',
        'attack_type',
        'area_of_effect_type',
        'area_of_effect_size',
        'damage',
        'school',
        'classes'
    ];

    /**
     * Get the school for the Spell
     */
    public function spell_school()
    {
        return $this->hasOne(SpellSchool::class,'index','school');
    }

    /**
     * Get the classes for the Spell
     */
    public function classes()
    {
        return $this->belongsToMany(DNDClass::class, 'classes_spells', 'spell_index', 'class_index', 'index');
    }

    public function getAoeTypeAttribute() {
        return $this->area_of_effect_type;
    }

    public function getAoeSizeAttribute() {
        return $this->area_of_effect_size;
    }
}
