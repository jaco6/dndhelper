<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DNDClass extends Model
{
    use HasFactory;

    protected $table = "classes";

    protected $primaryKey = 'index';
    public $incrementing = false;

    protected $fillable = [
        'index',
        'name',
    ];

    /**
     * Get all of the school for the Spell
     */
    public function spells()
    {
        return $this->belongsToMany(Spells::class, 'classes_spells', 'class_index', 'spell_index', 'index');
    }

    /**
     * Get all of the levels for the class
     */
    public function levels()
    {
        return $this->belongsToMany(DNDClassLevel::class,'class_class_levels','class_index','class_level_index');
    }
}
