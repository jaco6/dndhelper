<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpellSchool extends Model
{
    use HasFactory;

    protected $primaryKey = 'index';
    public $incrementing = false;

    protected $fillable = [
        'index',
        'name',
        'desc',
    ];

    /**
     * Get all of the school for the Spell
     */
    public function spells()
    {
        return $this->belongsToMany(Spells::class);
    }
}
